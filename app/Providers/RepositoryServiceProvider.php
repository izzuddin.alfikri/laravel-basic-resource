<?php

namespace App\Providers;

use App\Contracts\RepositoryInterface;
use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * List of keys of model name and it's repositories
     */
    private $repositories = [
        'akun-pengguna' => \App\UserRepository::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, function ($app) {
            $model_name = Route::getModelName();

            if($model_name && isset($this->repositories[$model_name])) {
                return $app->make($this->repositories[$model_name]);
            }
        });
    }
}
