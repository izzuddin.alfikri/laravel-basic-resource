<?php

namespace App;

use App\Contracts\RepositoryInterface;

class UserRepository implements RepositoryInterface
{
    public function all()
    {
        return User::all();
    }
}