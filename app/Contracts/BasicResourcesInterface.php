<?php

namespace App\Contracts;

interface BasicResourcesInterface
{
    public function index();

    public function show($id);

    public function store(RequestValidatorInterface $request);

    public function update(RequestValidatorInterface $request, $id);

    public function destroy($id);
}